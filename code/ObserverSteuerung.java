import java.util.Observable;
import java.util.Observer;

public class ObserverSteuerung extends BandSteuerung {

    @Override
    public void steuere() {
        Band b = getBand();
        b.open();

        BandObservable bandObservable = new BandObservable();
        BandObserver observer = new BandObserver();
        bandObservable.addObserver(observer);
        bandObservable.run();
    }

    enum StateEnum {
        ENDSTATE,
        STARTSTATE,
        NACHLINKS,
        NACHRECHTS,
        STOPLINKS,
        STOPRECHTS
    }

    class State {
        boolean leftLS;
        boolean rightLS;
        boolean leftT;
        boolean rightT;

        void reset() {
            leftLS = false;
            rightLS = false;
            leftT = false;
            rightT = false;
        }
    }

    class BandObservable extends Observable implements Runnable {

        void notifyObservers(State state) {
            super.notifyObservers(state);
        }

        @Override
        public void run() {
            Band band = getBand();

            // ugly but works :D
            while (true) {
                State state = new State();
                setChanged();

                if (band.getLinkeLS()) {
                    state.leftLS = true;
                }
                if (band.getRechteLS()) {
                    state.rightLS = true;
                }
                if (band.getRoteTaste()) {
                    state.leftT = true;
                }
                if (band.getSchwarzeTaste()) {
                    state.rightT = true;
                }
                notifyObservers(state);
            }
        }
    }

    class BandObserver implements Observer {

        StateEnum currentState = StateEnum.STARTSTATE;

        @Override
        public void update(Observable o, Object arg) {
            if (arg instanceof State) {
                State castedState = (State) arg;
                if (castedState.leftT && castedState.rightT) {
                    System.exit(0);
                }
                if (castedState.leftT) {
                    currentState = StateEnum.NACHLINKS;
                    getBand().setNachLinks();
                }
                if (castedState.rightT) {
                    currentState = StateEnum.NACHRECHTS;
                    getBand().setNachRechts();
                }

                switch (currentState) {
                    case STARTSTATE:
                        getBand().setStop();
                        if (castedState.leftLS) {
                            currentState = StateEnum.NACHRECHTS;
                        }
                        if (castedState.rightLS) {
                            currentState = StateEnum.NACHLINKS;
                        }
                        break;
                    case NACHLINKS:
                        getBand().setNachLinks();
                        if (castedState.leftLS) {
                            getBand().setStop();
                            currentState = StateEnum.STOPLINKS;
                        } else {
                            if (!getBand().warteAufLichtOderTastenAenderung(8000)) {
                                currentState = StateEnum.STARTSTATE;
                            }
                        }
                        break;
                    case NACHRECHTS:
                        getBand().setNachRechts();
                        if (castedState.rightLS) {
                            getBand().setStop();
                            currentState = StateEnum.STOPRECHTS;
                        } else {
                            if (!getBand().warteAufLichtOderTastenAenderung(8000)) {
                                currentState = StateEnum.STARTSTATE;
                            }
                        }
                        break;
                    case STOPLINKS:
                        getBand().setStop();
                        if (!castedState.leftLS) {
                            currentState = StateEnum.NACHLINKS;
                        }
                        break;
                    case STOPRECHTS:
                        getBand().setStop();
                        if (!castedState.rightLS) {
                            currentState = StateEnum.NACHRECHTS;
                        }
                        break;
                    case ENDSTATE:
                        System.exit(0);
                        break;
                }
            }
        }
    }
}
