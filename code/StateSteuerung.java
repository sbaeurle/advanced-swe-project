public class StateSteuerung extends BandSteuerung {
    private Context context;
    boolean leftLS = false;
    boolean rightLS = false;
    boolean rightT = false;
    boolean leftT = false;

    @Override
    public void steuere() {
        context = new Context(new Start());
        getBand().open();
        while (!context.state.getClass().getName().equals("End")) {
            if (rightT && leftT) {
                context.setState(new End());
                break;
            }
            if (rightT) {
                context.setState(new Right());
            }
            if (leftT) {
                context.setState(new Left());
            }
            leftLS = getBand().getLinkeLS();
            rightLS = getBand().getRechteLS();
            leftT = getBand().getRoteTaste();
            rightT = getBand().getSchwarzeTaste();
            context.request();
        }
        getBand().close();
    }

    interface State {
        void handle();
    }

    class Context {
        State state;

        Context(State s) {
            state = s;
        }

        void setState(State s) {
            state = s;
        }

        void request() {
            state.handle();
        }
    }

    class Start implements State {

        @Override
        public void handle() {
            getBand().setStop();
            if (leftLS) {
                context.setState(new Right());
            }
            if (rightLS) {
                context.setState(new Left());
            }
        }
    }

    class Right implements State {

        @Override
        public void handle() {
            getBand().setNachRechts();
            if (rightLS) {
                getBand().setStop();
                context.setState(new RightStop());
            } else {
                if (!getBand().warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }
    }

    class Left implements State {

        @Override
        public void handle() {
            getBand().setNachLinks();
            if (leftLS) {
                getBand().setStop();
                context.setState(new LeftStop());
            } else {
                if (!getBand().warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }
    }

    class LeftStop implements State {

        @Override
        public void handle() {
            if (!leftLS) {
                context.setState(new Right());
            }
        }
    }

    class RightStop implements State {

        @Override
        public void handle() {
            if (!rightLS) {
                context.setState(new Right());
            }
        }
    }

    class End implements State {

        @Override
        public void handle() {

        }
    }
}
