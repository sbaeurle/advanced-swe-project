public class TotmannSteuerung extends BandSteuerung {
    public void steuere() {
        final int timeout = 8000;
        Band b = getBand();
        ZustandsBandSteuerung.Zustand aktuellerZustand = ZustandsBandSteuerung.Zustand.STARTSTATE;
        boolean linkeLS;
        boolean rechteLS;
        boolean tasteR;
        boolean tasteL;

        while (!aktuellerZustand.equals(ZustandsBandSteuerung.Zustand.ENDSTATE)) {
            linkeLS = b.getLinkeLS();
            rechteLS = b.getRechteLS();
            tasteR = b.getSchwarzeTaste();
            tasteL = b.getRoteTaste();

            if (tasteL && tasteR) {
                aktuellerZustand = ZustandsBandSteuerung.Zustand.ENDSTATE;
            } else if (tasteL) {
                aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHLINKS;
            } else if (tasteR) {
                aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHRECHTS;
            }

            switch (aktuellerZustand) {
                case STARTSTATE:
                    b.setStop();
                    if (linkeLS) {
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHRECHTS;
                    }
                    if (rechteLS) {
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHLINKS;
                    }
                    break;
                case NACHLINKS:
                    b.setNachLinks();
                    if (linkeLS) {
                        b.setStop();
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.STOPLINKS;
                    } else {
                        if (!nachlaufen(true)) {
                            aktuellerZustand = ZustandsBandSteuerung.Zustand.STARTSTATE;
                        }
                    }
                    break;
                case NACHRECHTS:
                    b.setNachRechts();
                    if (rechteLS) {
                        b.setStop();
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.STOPRECHTS;
                    } else {
                        if (!nachlaufen(false)) {
                            aktuellerZustand = ZustandsBandSteuerung.Zustand.STARTSTATE;
                        }
                    }
                    break;
                case STOPLINKS:
                    if (!linkeLS) {
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHLINKS;
                    }
                    break;
                case STOPRECHTS:
                    if (!rechteLS) {
                        aktuellerZustand = ZustandsBandSteuerung.Zustand.NACHRECHTS;
                    }
                    break;
                case ENDSTATE:
                    System.exit(0);
                    break;
            }
        }
    }

    private boolean nachlaufen(boolean direction) {
        for (int i = 0; i < 16; i++) {
            if (direction) {
                getBand().setNachLinks();
            } else {
                getBand().setNachRechts();
            }
            if (getBand().warteAufLichtOderTastenAenderung(500)) {
                return true;
            }
        }
        return false;
    }


    enum Zustand {
        ENDSTATE,
        STARTSTATE,
        NACHLINKS,
        NACHRECHTS,
        STOPLINKS,
        STOPRECHTS
    }
}

