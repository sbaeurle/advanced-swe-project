public class VisitorSteuerung extends BandSteuerung {
    boolean leftLS = false;
    boolean rightLS = false;
    boolean rightT = false;
    boolean leftT = false;
    private Context context;

    @Override
    public void steuere() {
        context = new Context(new Start());
        getBand().open();
        while (!context.state.getClass().getName().equals("End")) {
            if (rightT && leftT) {
                context.setState(new End());
                break;
            }
            if (rightT) {
                context.setState(new Right());
            }
            if (leftT) {
                context.setState(new Left());
            }
            leftLS = getBand().getLinkeLS();
            rightLS = getBand().getRechteLS();
            leftT = getBand().getRoteTaste();
            rightT = getBand().getSchwarzeTaste();
            context.request();
        }
        getBand().close();
    }

    interface Visitor {
        void visitStart(Start obj);

        void visitLeft(Left obj);

        void visitRight(Right obj);

        void visitLeftStop(LeftStop obj);

        void visitRightStop(RightStop obj);

        void visitEnd(End obj);
    }

    interface Acceptor {
        String getName();

        void accept(Visitor v);
    }

    interface State {
        void handle();
    }

    class ConcrecteVisitor implements Visitor {

        @Override
        public void visitStart(Start obj) {
            System.out.println("Aktueller Zustand: " + obj.getName());
        }

        @Override
        public void visitLeft(Left obj) {
            System.out.println("Bewegung nach " + obj.getName());
        }

        @Override
        public void visitRight(Right obj) {
            System.out.println("Bewegung nach " + obj.getName());
        }

        @Override
        public void visitLeftStop(LeftStop obj) {
            System.out.println(obj.getName() + " gestoppt");
        }

        @Override
        public void visitRightStop(RightStop obj) {
            System.out.println(obj.getName() + " gestoppt");
        }

        @Override
        public void visitEnd(End obj) {
            System.out.println("Aktueller Zustand: " + obj.getName());
        }
    }

    class Context {
        State state;
        Visitor v = new ConcrecteVisitor();

        Context(State s) {
            state = s;
        }

        void setState(State s) {
            state = s;
        }

        void request() {
            ((Acceptor) state).accept(v);
            state.handle();
        }
    }

    class Start implements State, Acceptor {

        @Override
        public void handle() {
            getBand().setStop();
            if (leftLS) {
                context.setState(new Right());
            }
            if (rightLS) {
                context.setState(new Left());
            }
        }

        @Override
        public String getName() {
            return "Startzustand";
        }

        @Override
        public void accept(Visitor v) {
            v.visitStart(this);
        }
    }

    class Right implements State, Acceptor {

        @Override
        public void handle() {
            getBand().setNachRechts();
            if (rightLS) {
                getBand().setStop();
                context.setState(new RightStop());
            } else {
                if (!getBand().warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }

        @Override
        public String getName() {
            return "Rechts";
        }

        @Override
        public void accept(Visitor v) {
            v.visitRight(this);
        }
    }

    class Left implements State, Acceptor {

        @Override
        public void handle() {
            getBand().setNachLinks();
            if (leftLS) {
                getBand().setStop();
                context.setState(new LeftStop());
            } else {
                if (!getBand().warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }

        @Override
        public String getName() {
            return "Links";
        }

        @Override
        public void accept(Visitor v) {
            v.visitLeft(this);
        }
    }

    class LeftStop implements State, Acceptor {

        @Override
        public void handle() {
            if (!leftLS) {
                context.setState(new Right());
            }
        }

        @Override
        public String getName() {
            return "Links";
        }

        @Override
        public void accept(Visitor v) {
            v.visitLeftStop(this);
        }
    }

    class RightStop implements State, Acceptor {

        @Override
        public void handle() {
            if (!rightLS) {
                context.setState(new Right());
            }
        }

        @Override
        public String getName() {
            return "Rechts";
        }

        @Override
        public void accept(Visitor v) {
            v.visitRightStop(this);
        }
    }

    class End implements State, Acceptor {

        @Override
        public void handle() {

        }

        @Override
        public String getName() {
            return "Endzustand";
        }

        @Override
        public void accept(Visitor v) {
            v.visitEnd(this);
        }
    }
}

